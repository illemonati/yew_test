#[macro_use]
extern crate stdweb;

#[macro_use]
extern crate yew;

use stdweb::web::Date;
use yew::prelude::*;
use yew::services::ConsoleService;
use stdweb::web::IElement;
use stdweb::web::INonElementParentNode;
use stdweb::unstable::TryInto;

mod helpers;


pub struct Model {
    console: ConsoleService,
    last_done : String,
}

pub enum Msg {
    A0,
    A1,
    OddOrEven,
    Bulk(Vec<Msg>),
}

impl Component for Model {
    type Message = Msg;
    type Properties = ();

    fn create(_: Self::Properties, _: ComponentLink<Self>) -> Self {
        Model {
            console: ConsoleService::new(),
            last_done: String::from("opened this page"),
        }
    }

    fn update(&mut self, msg: Self::Message) -> ShouldRender {
        match msg {
            Msg::A0 => {
                self.last_done = String::from("Alert 0");
                self.console.log("Alert 0");
                stdweb::web::alert("0");
            }
            Msg::A1 => {
                self.last_done = String::from("Alert 1");
                self.console.log("Alert 1");
                stdweb::web::alert("1");
            }
            Msg::OddOrEven => {
                // let number : f64 = stdweb::web::document().get_element_by_id("n").unwrap().get_attribute("value").unwrap().parse::<f64>().unwrap();
                // let element : stdweb::web::html_element::InputElement = stdweb::web::document().get_element_by_id("n").unwrap();
                // let number : f64 = stdweb::web::document().get_element_by_id("n").unwrap().raw_value().parse::<f64>().unwarp();
                let number : String = js!{
                    return document.getElementById("n").value;
                }.try_into().unwrap();
                let number : i64 = match number.parse(){
                    Ok(n) => n,
                    Err(e) => {self.console.warn(format!("{:?}", e).as_str()); js!(document.getElementById("n").value = 0);0i64},
                };
                // let number : u64 = number.into();
                let odd_or_even = helpers::odd_or_even(number.into());
                self.console.log(format!("OddOrEven {} {}", number, odd_or_even).as_str());
                self.last_done = format!("OddOrEven {} {}", number, odd_or_even);
                stdweb::web::alert(format!("{} {}", number, odd_or_even).as_str());
            }
            Msg::Bulk(list) => for msg in list {
                self.update(msg);
                self.console.log("Bulk action");
            },
        }
        return true;
    }
}

impl Renderable<Model> for Model {
    fn view(&self) -> Html<Self> {
        html! {
            <div>
                <nav class="menu",>
                    <button onclick=|_| Msg::A0,> { "Alert 0" }</button>
                    <button onclick=|_| Msg::A1,> { "Alert 1" }</button>
                    <button onclick=|_| Msg::Bulk(vec![Msg::A1, Msg::A1]),>{ "Alert 1 but twice" }</button>
                    <button onclick=|_| Msg::Bulk(vec![Msg::A0, Msg::A0, Msg::A0,]),> {"Alert 0 but trice"} </button>
                </nav>
                <br />
                <input type="number", id="n", value="0", />
                <input type="submit", onclick=|_| Msg::OddOrEven, value="Odd or Even?", /> <br />
                <h1>{"The greatest Worst hello world there is !"}</h1>
                <hr />
                <p>{"What was done last: "}{ &self.last_done }</p>
                <p>{ Date::new().to_string() }</p>
            </div>
        }
    }
}
