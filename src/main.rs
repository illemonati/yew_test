extern crate yew;
extern crate yew_test;

use yew::prelude::*;
use yew_test::Model;

fn main() {
    yew::initialize();
    App::<Model>::new().mount_to_body();
    yew::run_loop();
}
