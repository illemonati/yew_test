pub fn odd_or_even(n: i64) -> &'static str{
    if (n & 1 == 1) {return "odd"} else {return "even"}
}
